"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createQ2TectonObservable = createQ2TectonObservable;

var _rxjs = require("rxjs");

/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function createQ2TectonObservable(q2Tecton) {
  var q2TectonObservable = q2Tecton;

  if (typeof q2TectonObservable.sources === 'undefined' || typeof q2TectonObservable.actions === 'undefined') {
    return q2TectonObservable;
  }

  q2TectonObservable.sources.requestExtensionData$ = function (requestOptions) {
    var _q2TectonObservable$s;

    if (typeof ((_q2TectonObservable$s = q2TectonObservable.sources) === null || _q2TectonObservable$s === void 0 ? void 0 : _q2TectonObservable$s.onRequestExtensionData) === 'function') {
      return (0, _rxjs.of)(q2TectonObservable.sources.requestExtensionData(requestOptions));
    }

    return (0, _rxjs.from)(q2TectonObservable.sources.requestExtensionData(requestOptions));
  };

  q2TectonObservable.sources.requestPlatformData$ = function (requestOptions) {
    var _q2TectonObservable$s2;

    if (typeof ((_q2TectonObservable$s2 = q2TectonObservable.sources) === null || _q2TectonObservable$s2 === void 0 ? void 0 : _q2TectonObservable$s2.onRequestPlatformData) === 'function') {
      return (0, _rxjs.of)(q2TectonObservable.sources.requestPlatformData(requestOptions));
    }

    return (0, _rxjs.from)(q2TectonObservable.sources.requestPlatformData(requestOptions));
  };

  q2TectonObservable.sources.isNavigable$ = function (featureName, moduleName) {
    var _q2TectonObservable$s3;

    if (typeof ((_q2TectonObservable$s3 = q2TectonObservable.sources) === null || _q2TectonObservable$s3 === void 0 ? void 0 : _q2TectonObservable$s3.onIsNavigable) === 'function') {
      return (0, _rxjs.of)(q2TectonObservable.sources.isNavigable(featureName, moduleName));
    }

    return (0, _rxjs.from)(q2TectonObservable.sources.isNavigable(featureName, moduleName));
  };

  q2TectonObservable.sources.fetchPlatformData$ = function (route) {
    var _q2TectonObservable$s4;

    if (typeof ((_q2TectonObservable$s4 = q2TectonObservable.sources) === null || _q2TectonObservable$s4 === void 0 ? void 0 : _q2TectonObservable$s4.onFetchPlatformData) === 'function') {
      return (0, _rxjs.of)(q2TectonObservable.sources.fetchPlatformData(route));
    }

    return (0, _rxjs.from)(q2TectonObservable.sources.fetchPlatformData(route));
  };

  return q2TectonObservable;
}
//# sourceMappingURL=index.js.map