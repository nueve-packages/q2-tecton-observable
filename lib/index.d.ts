/**
 * Copyright 2020 Nueve Solutions LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { IPlatformCoreAPI, ISources, TectonResponse } from 'q2-tecton-sdk';
import { IPlatformRequestBody } from 'q2-tecton-sdk/dist/esm/sources/requestPlatformData';
import { Observable } from 'rxjs';
import { IExtensionRequestBody, IExtensionResponse } from 'q2-tecton-sdk/dist/esm/sources/requestExtensionData';
export interface Sources extends ISources {
    requestExtensionData$?<R>(requestOptions?: IExtensionRequestBody): Observable<IExtensionResponse<R>>;
    requestPlatformData$?(requestOptions?: IPlatformRequestBody): Observable<TectonResponse>;
    isNavigable$?(featureName: string, moduleName?: string | undefined): Observable<Boolean>;
    fetchPlatformData$?(route: string): Observable<TectonResponse>;
}
export interface Q2TectonObservable extends IPlatformCoreAPI {
    sources?: Sources;
}
export declare function createQ2TectonObservable<T>(q2Tecton: T): T & Q2TectonObservable;
